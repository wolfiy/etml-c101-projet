/**
 * ETML
 * Auteur: Sébastien Tille
 * Date: 29.09.2023
 * Description: affiche, masque et gère l'opacité de la barre de navigation.
 */

/**
 * Décalage par rapport au haut de page.
 */
var oldScrollPos = window.scrollY;

/**
 * Hauteur de la barre de navigation.
 */
var navHeight = "-97px";

/**
 * Activer avec le scroll.
 */
window.onscroll = function() {
  hideShowNavbar();
}

/**
 * Afficher / masquer la bare de navigation.
 */
function hideShowNavbar() {
  var navbar = document.getElementById("navbar");
  var newScrollPos = window.scrollY;

  (oldScrollPos > newScrollPos || window.scrollY <= 100) ? 
    navbar.style.top = "0" : navbar.style.top = navHeight;
  oldScrollPos = newScrollPos;
}