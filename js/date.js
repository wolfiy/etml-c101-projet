/**
 * ETML
 * Auteur: Sébastien Tille
 * Date: 29.09.2023
 * Description: insère la date de modification du fichier.
 */

// Date de modification.
const LAST_MODIFICATION = new Date(document.lastModified);

// Date formatée au format suisse.
// https://developer.mozilla.org/en-US/docs/Web/API/Document/lastModified
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat
var formattedDate = new Intl.DateTimeFormat("fr-CH", { dateStyle: "medium"})
                            .format();

document.getElementById("last-modified-date").innerHTML = formattedDate;