# C101 - Projet Web Statique
Délais: 2 novembre 2023, 23:59

## Structure
La structure du répertoire suit les indications du cahier des
[charges](doc/E-P_WebStatique-ALL01-CdC.pdf).

## Règles suivies (application)
- Règle n° 176 - L'information n'est pas véhiculée uniquement par la couleur. (contrastes, *)
- Règle n° 152 - Les items actifs de menu sont signalés. (.active)
- Règle n° 161 - Le site est intégralement utilisable au clavier. (fonctionne)
- Règle n° 113 - Chaque image porteuse d'information est dotée d'une alternative textuelle appropriée. (oui, cf. w3c)
- Règle n° 133 - Les liens de même nature ont des couleurs, des formes et des comportements identiques sur toutes les
  pages. (boutons / footer)
- Règle n° 125 - Le code source de chaque page indique la langue principale du contenu. (cf. source)
- Règle n° 187 - Les mises en majuscules à des fins décoratives sont effectuées à l'aide des styles. (titres des cartes)
- Règle n° 189 - Le site propose un ou plusieurs mécanismes dédiés à l'adaptation aux terminaux mobiles. (mobile, tablette)
- Règle n° 4 - Les dates sont présentées dans des formats explicites. (footer)
- Règle n° 3 - Le code source de chaque page contient une métadonnée qui en décrit le contenu. (cf. source)

## Responsive
Le responsive a été testé en 1920x1080, 3440x1440, 3840x2160 et avec un Google Pixel 6.

## Notes à la main
Les notes manuscrites sont disponibles
[ici](https://1drv.ms/f/s!Aqyw3cvU5RWGhZFfeSTA4iACBnslQA).

## Sources
Logo NieR: Automata
https://en.wikipedia.org/wiki/File:Nier_logo.png

